var socket = io.connect('http://35.204.71.43:3000/', { 'forceNew': true });

socket.on('messages', function(data) {
  console.log(data);
  render(data);
  //window.open("https://www.w3schools.com");
  
});

socket.on('top-messages', function(data) {
  console.log('TOP messages:' + data);
  render(data);
  
});

socket.on('mqtt-messages', function(data) {
  console.log('mqtt messages:' + data);
  render2(data);
  
})


function render (data) {
  var html = data.map(function(elem, index) {
    if (elem==null){
      document.getElementById('iframe1').src='https://www.w3schools.com/tags/tag_iframe.asp';
    }
    else{
      document.getElementById('iframe1').src=elem.text;
    }
    return(`<div>
              <strong>${elem.author}</strong>:
              <em>${elem.text}</em>
            </div>`);
  }).join(" ");

  document.getElementById('messages').innerHTML = html;
}

function render2 (data) {
  var html = data.map(function(elem, index) {
    return(`<div>
              <strong>MENSAJE MQTT</strong>:
              <em>${elem.text}</em>
            </div>`);
  }).join(" ");

  document.getElementById('mqtt').innerHTML = html;
}






function addMessage(e) {
  var message = {
    author: document.getElementById('username').value,
    text: document.getElementById('texto').value
  };
  var top_message = {
    author: 'SYSTEM',
    text: 'THIS IS A TOP MESSAGE ,PLEASE RESET ALL SYSTEMS'
  };
  socket.emit('new-message', message);
//  socket.emit('top-message', top_message);
  return false;
}


function StopListening() {
  console.log('Stop listenning new messages');
  socket.off('messages'); 
}
function StartListening() {
  console.log('Start listenning new messages');
  socket.on('messages', function(data) {
    console.log(data);
    render(data);
   
    
  })
}

function Stopmqtt() {
  console.log('Stop mqtt queue');
  const Http = new XMLHttpRequest();
const url='http://35.204.71.43:3000/stopmqtt';
Http.open("GET", url);
Http.send();
Http.onreadystatechange=(e)=>{
 console.log('Respuesta para cola mqtt: ' + Http.responseText)
}
  
}

function Startmqtt() {
  console.log('Start mqtt queue');
  const Http = new XMLHttpRequest();
const url='http://35.204.71.43:3000/startmqtt';
Http.open("GET", url);
Http.send();
Http.onreadystatechange=(e)=>{
 console.log('Respuesta para cola mqtt: ' + Http.responseText)
}
  
}

function AddRedisKeyValue(form) {
  console.log('XXXXXX');
  var clave=document.getElementById("key").value;
  var valor=document.getElementById("value").value;
  var obj = { key: clave, value: valor};
  var myJSON = JSON.stringify(obj);
  //Generar json
  
  console.log('Add Redis ' + myJSON);

  const Http = new XMLHttpRequest();
  
const url='/AddRedisKeyValue';
Http.open("POST", url);
Http.setRequestHeader("Content-type", "application/json");
Http.send(myJSON);
Http.onreadystatechange=(e)=>{
 console.log('Respuesta para ENVIO REDIS: ' + Http.responseText)
}
  
}

function GetRedisKeyValue(form) {
  console.log('Get redis value');
  var clave=document.getElementById("key").value;
 // var valor=document.getElementById("value").value;
  var obj = { key: clave};
  var myJSON = JSON.stringify(obj);
  //Generar json
  
  console.log('Get Redis ' + myJSON);

  const Http = new XMLHttpRequest();
  
const url='/GetRedisKeyValue';
Http.open("POST", url);
Http.setRequestHeader("Content-type", "application/json");
Http.send(myJSON);
Http.onreadystatechange=(e)=>{
 console.log('Respuesta para ENVIO REDIS: ' + Http.responseText);
 var parsedData = JSON.parse(Http.responseText);
 document.getElementById("redisres").value=parsedData.valor;
 document.getElementById("redislista").innerHTML=parsedData.valor;
}
  
}

function GetMembersValue(form) {
  var parsedData ="";
  var lista=[];
  document.getElementById("redislista").innerHTML="";
  console.log('Get redis value');
  var clave=document.getElementById("key").value;
 // var valor=document.getElementById("value").value;
  var obj = { key: clave};
  var myJSON = JSON.stringify(obj);
  //Generar json
  
  console.log('Get Redis ' + myJSON);

  const Http = new XMLHttpRequest();
  const Http2 = new XMLHttpRequest();

  var url='/GetMembersValue';
  Http.open("POST", url);
  Http.setRequestHeader("Content-type", "application/json");
  Http.send(myJSON);
  url='http://localhost:3000/GetHgetAllValue';
  Http2.open("POST", url);
  Http2.setRequestHeader("Content-type", "application/json");
  Http.onreadystatechange=(e)=>{
   if (Http.readyState==4 && Http.status==200){
       parsedData = JSON.parse(Http.responseText);
       for (let item of parsedData) {
        
        document.getElementById("redislista").innerHTML +=item.identificador + "<BR>";
        lista.push(item.identificador);
        console.log(lista.length);

      }
     
    } 
  
  }  
  
  
}

function GetMembersValuePostman(form) {
  document.getElementById("redislista").innerHTML="";
  console.log('GetMembersValuePostman');
  var clave=document.getElementById("key").value;
  var obj = { key: clave};
  var myJSON = JSON.stringify(obj);
  var salida="";
  //Generar json
  

  const Http = new XMLHttpRequest();
  
const url='/GetMembersValuePostman';
Http.open("POST", url);
Http.setRequestHeader("Content-type", "application/json");
Http.send(myJSON);
Http.onreadystatechange=(e)=>{
  if (Http.readyState==4 && Http.status==200){
 console.log('Respuesta para ENVIO REDIS: ' + Http.responseText);
 var parsedData = JSON.parse(Http.responseText);
 var salida2=FormatTable(parsedData);
 console.log(salida2);
 document.getElementById("redislista").innerHTML=salida2;
}
}
  
}

function FormatTable(data){
  var cabecera="<table style=width:100% border=1><tr><th>Firstname</th><th>Lastname</th> <th>mail</th></tr>";
  var footer="</table>";
  var salida="";
  var linea="";
  
data.forEach(element => {
  linea="<tr><td>"+element.datos_persona.nombre +"</td><td>" +  element.datos_persona.apellidos +"</td><td>" + element.datos_persona.mail+"</td><tr>";
  salida+=linea;
});
 
   return cabecera+salida+footer;
}