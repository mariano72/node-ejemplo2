

var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var mqtt = require('mqtt');
var client  = mqtt.connect('mqtt://35.193.255.76');
var redis = require("redis");
const axios=require('axios');
var bodyParser = require('body-parser');
const translate = require('translate');

translate.engine = 'google';
translate.key = 'AIzaSyCI1u_Uy6ptBQwJNTLtleuVPieSjU1AeQA';
//Asincronia Redis
const bluebird = require('bluebird');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var clientredis = redis.createClient({host:'35.204.37.66',port:'6379',password:'SFukfyB7pTgG' });


const {promisify} = require('util');
const getAsync = promisify(clientredis.get).bind(clientredis);
const hgetallAsync = promisify(clientredis.hgetall).bind(clientredis);
const smembersAsync = promisify(clientredis.smembers).bind(clientredis);

//Tema Dialogglow google cloud
const functions = require('firebase-functions');
const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');
//const agent = new WebhookClient({ request, response });
//Fin Dialogglow google cloud

//inicio REDIS

clientredis.on("error", function (err) {
  console.log("Error " + err);
});

clientredis.set("owner", "nanonano", redis.print);
clientredis.set("features","adm",redis.print);
clientredis.hmset("usuario1","nombre","mariano","apellidos","morales",redis.print);

//Inicio MQTT
client.on('connect', function () {
  client.subscribe('test', function (err) {
    if (!err) {
      console.log('Me suscribo a Topic TEST');
    }
  })
});
//fin MQTT
/*
//Inicio recepción topico test MQTT
client.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString());
  //client.end();
});
//fin MQTT
*/
var messages = [{
  id: 1,
  text: "Primer mensaje",
  author: "Mariano Morales"
}];
var top_messages = [{
  id: 1,
  text: "",
  author: ""
}];
var mqtt_mensaje= [{
  text: "sasassasa",
  author: "asasassaa"
}];


// parse application/x-www-form-urlencoded
app.use(bodyParser.json());

//Peticiones http
app.use(express.static('public'));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.get('/hello', function(req, res) {
  res.status(200).send("Hello World!");
});
app.get('/axios', function(req, res) {
  axios.get(`http://www.omdbapi.com/?apikey=31b8e4a9&s=casablanca`)
  .then(response => {
    for (var contador in response.data.Search){
      console.log(response.data.Search[contador].Title);
      console.log(response.data.Search[contador].Year);
    }
 
     //agent.add(`Año estreno`);
   //  agent.add(response.data.Search[0].Year);
  })
  .catch(error => {
    console.log(error);
    //agent.add(`Error en la llamada` + pelicula);  
  });
  
   
  res.status(200).send("AXIOS OK");
});

app.get('/pokemon', function(req, res) {
 
  axios.get(`https://pokeapi.co/api/v2/pokemon/charmander`)
  .then(async response => {
    for (var contador in response.data.abilities){
      console.log(response.data.abilities[contador].ability.name);
      const traduccion = await translate(response.data.abilities[contador].ability.name, 'es');
      console.log(traduccion);
     // console.log(response.data.Search[contador].Year);
    }
 
     //agent.add(`Año estreno`);
   //  agent.add(response.data.Search[0].Year);
  })
  .catch(error => {
    console.log(error);
    //agent.add(`Error en la llamada` + pelicula);  
  });
  
   
  res.status(200).send("pokemon OK");
});

app.get('/pokemon2', function(req, res) {
 
  axios.get(`https://pokeapi.co/api/v2/pokemon/charmander`)
  .then(async response => {
    for (var contador in response.data.moves){
      //console.log(response.data.moves[contador].move.name);
      const traduccion = await translate(response.data.moves[contador].move.name, 'es');
      console.log(traduccion);
     // console.log(response.data.Search[contador].Year);
    }
 
     //agent.add(`Año estreno`);
   //  agent.add(response.data.Search[0].Year);
  })
  .catch(error => {
    console.log(error);
    //agent.add(`Error en la llamada` + pelicula);  
  });
  
   
  res.status(200).send("pokemon OK");
});


app.get('/ade', function(req, res) {
 
  axios.get(`https://customer-preprod.adegroup.eu/webservice.php/clinic.json?token=cd60e67a90dbbfb92df93394dc48f3d5efafdf09&postal_code=17220`)
  .then(response => {
    var code="11111";
    code.toLowerCase()


    for (var contador in response.data){
      console.log(response.data[contador].clinic_name.toLowerCase());
      console.log(response.data[contador].city_name.toLowerCase());
      console.log(response.data[contador].clinic_address);
      
     // console.log(response.data.Search[contador].Year);
    }
 
     //agent.add(`Año estreno`);
   //  agent.add(response.data.Search[0].Year);
  })
  .catch(error => {
    console.log(error);
    //agent.add(`Error en la llamada` + pelicula);  
  });
  
   
  res.status(200).send("pokemon OK");
});






app.get('/stopmqtt', function(req, res) {
  console.log('Petición desde el front para parar cola MQTT');
  client.end();
});
app.get('/startmqtt', function(req, res) {
  console.log('Petición desde el front para iniciar cola MQTT');
  client.reconnect();
});
app.post('/AddRedisKeyValue', function(req, res) {
   console.log('Petición desde el front para add redis keys ' + req.body.text);

  clientredis.set(req.body.key ,req.body.value,redis.print);
});

app.post('/GetRedisKeyValue', function(req, res) {
  console.log('Petición desde el front para get redis keys ' + req.body.text);
  
  clientredis.get(req.body.key, function(err, reply) {
    // reply is null when the key is missing
    
    console.log(reply);
    res.setHeader('Content-Type', 'application/json');
    res.json({valor:reply});
});

//  clientredis.get(req.body.key,);
  
});

app.post('/GetMembersValue', function(req, res) {

  var jsonArr=[];
  res.setHeader('Content-Type', 'application/json');
  
  console.log('Petición desde el front para members redis keys ' + req.body.text);
  clientredis.smembers(req.body.key, function(err, reply) {
    // reply is null when the key is missing
    for (var i = 0; i < reply.length; i++) {
       jsonArr.push({identificador:reply[i]});
    }

    console.log("Lista de items " + reply);
    respuesta=JSON.stringify(jsonArr);
    console.log(respuesta);
    res.send(respuesta);
   });
});

app.post('/GetHgetAllValue', function(req, res) {
  
  var respuestastring="";
  res.setHeader('Content-Type', 'application/json');
  
  console.log('Petición desde el front para HGETALL redis keys ');
  
    // reply is null when the key is missing
   
    hgetallAsync(req.body.key).then(function(respuestastring) {
      respuestastring   =JSON.stringify(respuestastring);
      console.log(respuestastring);
      res.send(respuestastring);
     }).catch(e=>{console.log('error ',e)});

   
   
      /* asincrono
      clientredis.hgetall(reply[i],function(err, resp) {
         console.log( resp);
         respuestastring   =JSON.stringify(resp);
         console.log( respuestastring);         
         
      })
        */   
    
  
});



app.post('/GetMembersValuePostman', async function(req, res) {

  var jsonArr=[];
  var datos_persona="";
  
  res.setHeader('Content-Type', 'application/json');
  console.log("xxxx");
  //console.log(req.body.key);
  console.log(req.body);
  console.log("xxxx");
  
 // clientredis.set("owner", "nanodssssssssssddddnano", redis.print);

  var valor = await clientredis.getAsync("owner");
  console.log(valor);
  valor = await clientredis.getAsync("feature");
  console.log(valor);
  var lista=await clientredis.smembersAsync("9332ckn");
  var lista=await clientredis.smembersAsync(req.body.key);
  
  for (var i = 0; i < lista.length; i++) {
    //jsonArr.push({identificador:lista[i]});
    datos_persona=await clientredis.hgetallAsync(lista[i]);
    jsonArr.push({datos_persona});
    console.log(datos_persona);
 }
  console.log(lista);
  console.log('Petición desde el front para members redis keys ');
 /* llamada asincrona
  clientredis.smembers(req.body.key, function(err, reply) {
    // reply is null when the key is missing
    for (var i = 0; i < reply.length; i++) {           
       jsonArr.push({identificador:reply[i]});
    }

    console.log("Lista de items " + reply);
    respuesta=JSON.stringify(jsonArr);
    console.log(respuesta);
    res.send(respuesta);
   
  });
  */
 respuesta=JSON.stringify(jsonArr);
 res.send(respuesta);
});


//Fin peticiones http



io.on('connection', function(socket) {
  console.log('Alguien se ha conectado con Sockets');
  socket.emit('messages', messages);

  socket.on('new-message', function(data) {
    messages.push(data);
    console.log('ENVIO NUEVO MENSAJE' +data);
    console.log (messages);
    io.sockets.emit('messages', messages);
  });

  socket.on('disconnect', function () {
    console.log('A user disconnected');
 });
 
 //Canales Especiales 
  socket.on('top-message', function(data) {
    top_messages.push(data);
    console.log('ENVIO NUEVO TOP MENSAJE');
    io.sockets.emit('top-messages', top_messages);
  });
  
  //Inicio recepción topico test MQTT
client.on('message', function (topic, mensaje) {
  // message is Buffer
  mqtt_mensaje = [{
    author: "MENSAJE MQTT",
    text: mensaje.toString('utf8')
  }];
  console.log(mqtt_mensaje);
  //mqtt_mensaje.push(mensaje);
  console.log('Voy a intentar emitir por un socket la recepción de un topico ' + mensaje);
  io.sockets.emit('mqtt-messages', mqtt_mensaje);
  });
 

//fin MQTT
 
});


server.listen(3000, function() {
  console.log("Servidor corriendo en http://localhost:3000");
});